import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyD1sGJvLwL4pXAFPYau6ZFmfRqYgGKE3qM",
  authDomain: "aut-core.firebaseapp.com",
  databaseURL: "https://aut-core.firebaseio.com",
  projectId: "aut-core",
  storageBucket: "aut-core.appspot.com",
  messagingSenderId: "1075892092374",
  appId: "1:1075892092374:web:25552123ef22b3a91d3877",
  measurementId: "G-ZV62KNE7HV"
};

export const app = firebase.initializeApp(config);
export const functions = firebase.functions();
export const database = firebase.firestore();